import org.junit.After;
import org.junit.Assert;
import org.junit.Before;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;


public class demoTestingPageTest {
    WebDriver driver;
    Actions actions;
    WebDriverWait webDriverWait;
    demoTestingPageMenuItems demoTestingPageMenuItems;
    demoTestingPageDatePicker demoTestingPageDatePicker;
    demoTestingPageProgressBar demoTestingPageProgressBar;


    @Before
    public void initialize() {
        String URL = "https://www.globalsqa.com";
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\Desktop\\IBATECH\\IBAProject\\src\\main\\resources\\chromedriver.exe");

        driver = new ChromeDriver();
        driver.get(URL);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        actions = new Actions(driver);
        demoTestingPageMenuItems = new demoTestingPageMenuItems(driver);
        demoTestingPageDatePicker = new demoTestingPageDatePicker(driver);
        demoTestingPageProgressBar = new demoTestingPageProgressBar(driver);
        webDriverWait = new WebDriverWait(driver, 15);

    }

    @Test
    public void verifyingEqualityOfColumnSize() {
        demoTestingPageMenuItems.moveToDemoTestingSite(actions);
        int expectedSizeOfColumnElements = 6;
        Assert.assertTrue("Each column equals to expected size of column elements ", demoTestingPageMenuItems.sizeOfFirstColumn() == expectedSizeOfColumnElements && demoTestingPageMenuItems.sizeOfSecondColumn() == expectedSizeOfColumnElements && demoTestingPageMenuItems.sizeOfThirdColumn() == expectedSizeOfColumnElements);

    }

    @Test
    public void verifyDateFormat() {
        demoTestingPageDatePicker.moveToDatePickerPage(actions);
        demoTestingPageDatePicker.clickDatePicker();
        Assert.assertTrue(demoTestingPageDatePicker.validateDate(demoTestingPageDatePicker.getDataFormat()));


    }

    @Test
    public void checkDownloadButtonAndProcess() {
        demoTestingPageProgressBar.moveToProgressBar(actions);
        demoTestingPageProgressBar.clickDownloadButton();
      //  System.out.println(demoTestingPageProgressBar.getTextOfProcess(webDriverWait));
        Assert.assertTrue("Completed successfully!", demoTestingPageProgressBar.getTextOfProcess(webDriverWait));


    }

    @After
    public void finish(){
        driver.quit();
    }

}





