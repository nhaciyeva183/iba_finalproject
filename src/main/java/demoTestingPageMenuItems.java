import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class demoTestingPageMenuItems extends pageObjectPattern {

    public demoTestingPageMenuItems(WebDriver driver) {
        super(driver);
    }


    @FindBy(xpath = "//div[@id='menu']/ul/li[4]")
    public WebElement testerHub;

    @FindBy(xpath = "//div[@id=\"menu\"]/ul/li[4]/div/ul/li[1]/a")
    public WebElement demoTestingSite;

    @FindBy(xpath = "//div[@class='price_column '][1]/ul/li[@class='price_footer']")
    List<WebElement> firstColumn;


    @FindBy(xpath = "//div[@class='price_column '][2]/ul/li[@class='price_footer']")
    List<WebElement> secondColumn;

    @FindBy(xpath = "//div[@class='price_column '][3]/ul/li[@class='price_footer']")
    List<WebElement> thirdColumn;


    public void moveToDemoTestingSite(Actions actions) {
        actions.moveToElement(testerHub).moveToElement(demoTestingSite).click().perform();
    }

    public int sizeOfFirstColumn() {
        return firstColumn.size();
    }

    public int sizeOfSecondColumn() {
        return secondColumn.size();
    }

    public int sizeOfThirdColumn() {
        return thirdColumn.size();
    }

}
