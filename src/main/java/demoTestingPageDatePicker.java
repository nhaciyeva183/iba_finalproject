import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class demoTestingPageDatePicker extends pageObjectPattern{


    public demoTestingPageDatePicker(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[@id='menu']/ul/li[4]")
    public WebElement testerHub;

    @FindBy(xpath = "//div[@id=\"menu\"]/ul/li[4]/div/ul/li[1]/a")
    public WebElement demoTestingSite;

    @FindBy(xpath = "//div[@id='menu']/ul/li[4]/div/ul/li/div/ul/li[@id='menu-item-2827']")
    public WebElement datePickerPage;

    @FindBy(css = ".demo-frame.lazyloaded")
    public WebElement datePickerIFrame;


    @FindBy(xpath = "//input[@id=\"datepicker\"]")
    public WebElement datePicker;


    @FindBy(xpath = "//div[@id=\"ui-datepicker-div\"]/div/a[2]")
    public WebElement nextMonth;

    @FindBy(xpath = "//div[@id=\"ui-datepicker-div\"]/table/tbody/tr[5]/td[6]/a")
    public WebElement currentDayOfNextMonth;


    public void moveToDatePickerPage(Actions actions) {
        actions.moveToElement(testerHub).moveToElement(demoTestingSite).moveToElement(datePickerPage).click().perform();
    }


    public void clickDatePicker() {
        driver.switchTo().frame(datePickerIFrame);
        datePicker.click();
    }


    public String getDataFormat() {
        nextMonth.click();
        currentDayOfNextMonth.click();
        return datePicker.getAttribute("value");
    }


    public boolean validateDate(String strDate) {
        if (strDate.trim().equals("")) {
            return true;
        } else {
            SimpleDateFormat sdfrmt = new SimpleDateFormat("MM/dd/yyyy");
            sdfrmt.setLenient(false);
            try {
                Date javaDate = sdfrmt.parse(strDate);
                System.out.println(strDate + " is valid date format");
            } catch (ParseException e) {
                System.out.println(strDate + " is Invalid Date format");
                return false;
            }
            return true;
        }

    }


}
