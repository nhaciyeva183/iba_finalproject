import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class pageObjectPattern {
    WebDriver driver;

    public pageObjectPattern(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
}
