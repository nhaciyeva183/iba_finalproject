import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class demoTestingPageProgressBar extends pageObjectPattern {

    public demoTestingPageProgressBar(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[@id='menu']/ul/li[4]")
    public WebElement testerHub;

    @FindBy(xpath = "//div[@id=\"menu\"]/ul/li[4]/div/ul/li[1]/a")
    public WebElement demoTestingSite;

    @FindBy(xpath = "//div[@id='menu']/ul/li[4]/div/ul/li/div/ul/li[@id='menu-item-2832']")
    public WebElement progressBar;

    @FindBy(xpath = "//button[@id='downloadButton']")
    public WebElement downloadButton;

    @FindBy(css = ".demo-frame.lazyloaded")
    public WebElement downloadIFrame;

    By linkCompletedDownload = By.xpath("//*[@id=\"dialog\"]/div[1]");

    public void moveToProgressBar(Actions actions) {
        actions.moveToElement(testerHub).moveToElement(demoTestingSite).moveToElement(progressBar).click().perform();
    }

    public void clickDownloadButton() {
        driver.switchTo().frame(downloadIFrame);
        downloadButton.click();
    }

    public boolean getTextOfProcess(WebDriverWait webDriverWait) {
        webDriverWait.until(ExpectedConditions.textToBe(linkCompletedDownload, "Complete!"));
        return true;
    }
}
